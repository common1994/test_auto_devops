#########################################################################
# File Name:    build.sh
# Author:       common
# Mail:         yp_abc2015@163.com
# Created Time: Mon 27 Nov 2017 07:06:21 PM CST
#########################################################################

#!/bin/bash

work_dir=`pwd`

mkdir -p $work_dir/build
cd $work_dir/build

cmake .. -G"Unix Makefiles"
sleep 5
make
sleep 5
cp $work_dir/build/linux/bin/test_auto_devops ../
cd ../
